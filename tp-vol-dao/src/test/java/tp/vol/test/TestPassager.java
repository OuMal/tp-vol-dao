package tp.vol.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import tp.vol.Passager;
import tp.vol.enumeration.Civilite;
import tp.vol.enumeration.TypePI;
import tp.vol.singleton.Application;

public class TestPassager {

	public static void main(String[] args) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		
		Passager belette = new Passager();
		
		belette.setId(20L);
		belette.setNom("PETITE");
		belette.setPrenoms("Belette");
		try {
			belette.setDtNaissance(sdf.parse("27-09-1993"));
			belette.setDateValiditePI(sdf.parse("27-09-2021"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		belette.setNumIdentite("zertyu");
		belette.setNationalite("Française");
		belette.setCivilite(Civilite.MME);
		belette.setTypePI(TypePI.PASS);
		belette.setMail("belette@petite.com");
		belette.setTelephone("0258665466");
		
		Application.getInstance().getPassagerDao().create(belette);

	}

}
