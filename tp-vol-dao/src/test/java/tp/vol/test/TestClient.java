package tp.vol.test;

import tp.vol.Adresse;
import tp.vol.Client;
import tp.vol.enumeration.Civilite;
import tp.vol.enumeration.MoyenPaiement;
import tp.vol.enumeration.TypeClient;
import tp.vol.enumeration.TypeEntreprise;
import tp.vol.singleton.Application;

public class TestClient {

	public static void main(String[] args) {
		
		Adresse adresseFacturation = new Adresse();
		adresseFacturation.setAdresseId(1L);
		adresseFacturation.setVoie("2, allée des renards");
		adresseFacturation.setComplement("Résidence les Roseaux");
		adresseFacturation.setCodePostal("64600");
		adresseFacturation.setVille("Anglet");
		adresseFacturation.setPays("France");
		
		Application.getInstance().getAdresseDao().create(adresseFacturation);
		
		Client fennec = new Client();
		fennec.setClientId(1L);
		fennec.setMail("fennecdu33@hotmail.com");
		fennec.setTelephone("0612584975");
		fennec.setMoyenPaiement(MoyenPaiement.CB);
		fennec.setCivilite(Civilite.NSP);
		fennec.setNom("du Sahara");
		fennec.setPrenom("Fennec");
		fennec.setNumSiret("grfdgsth5");
		fennec.setNomEntreprise("Vulpes zerda");
		fennec.setNumTva("20");
		fennec.setTypeEntreprise(TypeEntreprise.SA);
		fennec.setTypeClient(TypeClient.PART);
		fennec.setAdresseFacturation(adresseFacturation);
		
		Application.getInstance().getClientDao().create(fennec);
			
		System.out.println(fennec.getCivilite() + " " + fennec.getPrenom()+" " + fennec.getNom() + " " + fennec.getTypeClient() + " " + fennec.getTypeEntreprise());
		
//		Application.getInstance().getClientDao().update(fennec);
//		
//		fennec = Application.getInstance().getClientDao().findById(1L);
//		
//		System.out.println(fennec.getNom()+" " + fennec.getPrenom());
//		
//		Application.getInstance().getClientDao().delete(fennec);
//		
//		fennec = Application.getInstance().getClientDao().findById(1L);
//		
//		System.out.println(fennec);
	}

}
