package tp.vol.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import tp.vol.Client;
import tp.vol.Passager;
import tp.vol.Reservation;
import tp.vol.Voyage;
import tp.vol.enumeration.StatutReserv;
import tp.vol.singleton.Application;

public class TestReservation {

	public static void main(String[] args) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		
		Passager belette = Application.getInstance().getPassagerDao().findById(20L);
		Client client = Application.getInstance().getClientDao().findById(2L);
		Voyage voy = Application.getInstance().getVoyageDao().findById(1L);
		
		Reservation reserv = new Reservation();
		
		reserv.setNumero("7");
		reserv.setStatut(StatutReserv.PAY);
		reserv.setTarif(1000.0f);
		reserv.setTauxTVA(0.15f);
		try {
			reserv.setDateReservation(sdf.parse("18-10-2019"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		reserv.setPassager(belette);
		reserv.setClient(client);
		reserv.setVoyage(voy);
		
		
		
		Application.getInstance().getReservationDao().create(reserv);

	}

}
