package tp.vol;

import java.text.ParseException;
import java.text.SimpleDateFormat;


import tp.vol.singleton.Application;

public class TestVolDaoSql {

	public static void main(String[] args) {
		
		Compagnie af = new Compagnie ();
		af = Application.getInstance().getCompagnieDao().findById("Air France");
		
		Aeroport bdx = new Aeroport ();
		bdx = Application.getInstance().getAeroportDao().findById("BOD");
		
		Aeroport gtw = new Aeroport ();
		gtw = Application.getInstance().getAeroportDao().findById("GTW");
		
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		Vol vol4 = new Vol();
		vol4.setId(4L);
		try {
			vol4.setDateDepart(sdf.parse("22/09/2019"));
			vol4.setDateArrivee(sdf.parse("23/09/2019"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		vol4.setOuvert(true);
		vol4.setNbPlaces(200);
		vol4.setAeroportDepart(bdx);
		vol4.setAeroportArrivee(gtw);
		vol4.setCompagnie(af);
		
		
		
		Application.getInstance().getVolDao().create(vol4);
		
		Application.getInstance().getVolDao().findAll();
		
		System.out.println(vol4.getId()+" " +vol4.getAeroportDepart()+" " +vol4.getAeroportArrivee()+" "+vol4.getCompagnie());
		
//		vol4.setNbPlaces(100);
//		
//		
//		Application.getInstance().getVolDao().update(vol4);
//
//		vol4 = Application.getInstance().getVolDao().findById(4L);
//		
//		System.out.println(vol4.getId()+" " +vol4.getDateDepart()+" " +vol4.getDateArrivee()+" "+vol4.getNbPlaces());
//		
//		Application.getInstance().getVolDao().delete(vol4);
//		
//		vol4 = Application.getInstance().getVolDao().findById(4L);
//		
//		System.out.println(vol4);

		
		
		

	}

}
