package tp.vol.dao.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import tp.vol.Adresse;
import tp.vol.Client;
import tp.vol.dao.IClientDao;
import tp.vol.enumeration.Civilite;
import tp.vol.enumeration.MoyenPaiement;
import tp.vol.enumeration.TypeClient;
import tp.vol.enumeration.TypeEntreprise;
import tp.vol.singleton.Application;

public class ClientDaoSql implements IClientDao{

	@Override
	public List<Client> findAll() {
		List<Client> clients = new ArrayList<Client>();
		
		Connection connection = null;
		
		try {
			connection = Application.getInstance().getConnection();
			
			PreparedStatement ps = connection.prepareStatement("SELECT client_id, mail, telephone, moyen_paiement, civilite, nom, prenoms, num_siret, nom_entreprise, num_tva, type_entreprise, type_client, adresse_facturation FROM clients");
			
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				Long client_id = rs.getLong("client_id");
				String mail = rs.getString("mail");
				String telephone = rs.getString("telephone");
				String moyen_paiement = rs.getString("moyen_paiement");
				String civilite = rs.getString("civilite");
				String nom = rs.getString("nom");
				String prenoms = rs.getString("prenoms");
				String num_siret = rs.getString("num_siret");
				String nom_entreprise = rs.getString("nom_entreprise");
				String num_tva = rs.getString("num_tva");
				String type_entreprise = rs.getString("type_entreprise");
				String type_client = rs.getString("type_client");
				Long clientAdresse = rs.getLong("adresse_facturation");

				Client client = new Client();
				client.setClientId(client_id);
				client.setMail(mail);
				client.setTelephone(telephone);
				client.setMoyenPaiement(MoyenPaiement.valueOf(moyen_paiement));
				client.setCivilite(Civilite.valueOf(civilite));
				client.setNom(nom);
				client.setPrenom(prenoms);
				client.setNumSiret(num_siret);
				client.setNomEntreprise(nom_entreprise);
				client.setNumTva(num_tva);
				client.setTypeEntreprise(TypeEntreprise.valueOf(type_entreprise));
				client.setTypeClient(TypeClient.valueOf(type_client));
				
				if (clientAdresse != null) {
					Adresse adresse_id = Application.getInstance().getAdresseDao().findById(clientAdresse);
					client.setAdresseFacturation(adresse_id);
				}
				
				clients.add(client);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return clients;
	
	}

	@Override
	public Client findById(Long client_id) {
		
		Client client = null;
		
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT client_id, mail, telephone, moyen_paiement, civilite, nom, prenoms, num_siret, nom_entreprise, num_tva, type_entreprise, type_client, adresse_facturation FROM clients WHERE client_id = ?");

			ps.setLong(1, client_id);
			
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				String mail = rs.getString("mail");
				String telephone = rs.getString("telephone");
				String moyen_paiement = rs.getString("moyen_paiement");
				String civilite = rs.getString("civilite");
				String nom = rs.getString("nom");
				String prenoms = rs.getString("prenoms");
				String num_siret = rs.getString("num_siret");
				String nom_entreprise = rs.getString("nom_entreprise");
				String num_tva = rs.getString("num_tva");
				String type_entreprise = rs.getString("type_entreprise");
				String type_client = rs.getString("type_client");
				Long clientAdresse = rs.getLong("adresse_facturation");

				client = new Client();
				client.setClientId(client_id);
				client.setMail(mail);
				client.setTelephone(telephone);
				client.setMoyenPaiement(MoyenPaiement.valueOf(moyen_paiement));
				client.setCivilite(Civilite.valueOf(civilite));
				client.setNom(nom);
				client.setPrenom(prenoms);
				client.setNumSiret(num_siret);
				client.setNomEntreprise(nom_entreprise);
				client.setNumTva(num_tva);
				client.setTypeEntreprise(TypeEntreprise.valueOf(type_entreprise));
				client.setTypeClient(TypeClient.valueOf(type_client));

				if (clientAdresse != null) {
					Adresse adresse_id = Application.getInstance().getAdresseDao().findById(clientAdresse);
					client.setAdresseFacturation(adresse_id);
				}
				
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return client;
	}

	@Override
	public void create(Client obj) {
		
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("INSERT INTO clients (client_id, mail, telephone, moyen_paiement, civilite, nom, prenoms, num_siret, nom_entreprise, num_tva, type_entreprise, type_client, adresse_facturation) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");
			
			ps.setLong(1, obj.getClientId());
			ps.setString(2, obj.getMail().toString());
			ps.setString(3, obj.getTelephone().toString());
			ps.setString(4, obj.getMoyenPaiement().toString());
			ps.setString(5, obj.getCivilite().toString());
			ps.setString(6, obj.getNom());
			ps.setString(7, obj.getPrenom());
			ps.setString(8, obj.getNumSiret());
			ps.setString(9, obj.getNomEntreprise());
			ps.setString(10, obj.getNumTva());
			ps.setString(11, obj.getTypeEntreprise().toString());
			ps.setString(12, obj.getTypeClient().toString());

			if(obj.getAdresseFacturation() != null && obj.getAdresseFacturation().getAdresseId() != null) {
				ps.setLong(13, obj.getAdresseFacturation().getAdresseId());
			} else {
				ps.setNull(13, Types.INTEGER);
			}
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void update(Client obj) {
		
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("UPDATE clients SET mail = ?, telephone = ?, moyen_paiement = ?, civilite = ?, nom = ?, prenoms = ?, num_siret = ?, nom_entreprise = ?, num_tva = ?, type_entreprise = ?, type_client = ?, adresse_facturation = ? WHERE client_id = ?");
			
			ps.setString(1, obj.getMail());
			ps.setString(2, obj.getTelephone());
			ps.setString(3, obj.getMoyenPaiement().toString());
			ps.setString(4, obj.getCivilite().toString());
			ps.setString(5,  obj.getNom());
			ps.setString(6, obj.getPrenom());
			ps.setString(7, obj.getNumSiret());
			ps.setString(8, obj.getNomEntreprise());
			ps.setString(9, obj.getNumTva());
			ps.setString(10, obj.getTypeEntreprise().toString());
			ps.setString(11, obj.getTypeClient().toString());
			ps.setLong(12, obj.getClientId());
			
			if(obj.getAdresseFacturation() != null && obj.getAdresseFacturation().getAdresseId() != null) {
				ps.setLong(13, obj.getAdresseFacturation().getAdresseId());
			} else {
				ps.setNull(13, Types.INTEGER);
			}
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void delete(Client obj) {
		deleteById(obj.getClientId());
	}

	@Override
	public void deleteById(Long id) {
		
		Connection connection = null;
		
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE clients WHERE client_id = ?");

			ps.setLong(1, id);
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
