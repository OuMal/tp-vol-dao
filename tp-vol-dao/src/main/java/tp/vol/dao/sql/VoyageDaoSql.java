package tp.vol.dao.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tp.vol.Voyage;
import tp.vol.dao.IVoyageDao;
import tp.vol.singleton.Application;

public class VoyageDaoSql implements IVoyageDao {

	@Override
	public List<Voyage> findAll() {
		List<Voyage> voyages = new ArrayList<Voyage>();

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT voyage_id FROM voyages");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Long id = rs.getLong("voyage_id");

				Voyage voyage = new Voyage();
				voyage.setId(id);
				voyages.add(voyage);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return voyages;

	}

	@Override
	public Voyage findById(Long id) {
		Voyage voyage = null;

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT voyage_id FROM voyages WHERE voyage_id = ?");

			ps.setLong(1, id);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {

				voyage = new Voyage();
				voyage.setId(id);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return voyage;
	}

	@Override
	public void create(Voyage obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("INSERT INTO voyages (vol_id) VALUES (?)");

			ps.setLong(1, obj.getId());

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void update(Voyage obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("UPDATE voyages SET WHERE voyage_id = ?");

			ps.setLong(5, obj.getId());

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void delete(Voyage obj) {
		deleteById(obj.getId());

	}

	@Override
	public void deleteById(Long id) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE voyages WHERE voyage_id = ?");

			ps.setLong(1, id);

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

}
