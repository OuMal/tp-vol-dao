package tp.vol.dao.sql;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import tp.vol.Client;
import tp.vol.Passager;
import tp.vol.Reservation;
import tp.vol.Voyage;
import tp.vol.dao.IReservationDao;
import tp.vol.enumeration.StatutReserv;
import tp.vol.singleton.Application;

public class ReservationDaoSql implements IReservationDao{

	@Override
	public List<Reservation> findAll() {
		List<Reservation> reservations = new ArrayList<Reservation>();

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT num_reservation, statut, tarif, taux_tva, date_reservation, passager_id, client_id, voyage_id FROM reservations");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				String num_reservation = rs.getString("num_reservation");
				String statut = rs.getString("statut");
				Float tarif = rs.getFloat("tarif");
				Float taux_tva = rs.getFloat("taux_tva");
				Date date_reservation = rs.getDate("date_reservation");
				Long passager_id = rs.getLong("passager_id");
				Long client_id = rs.getLong("client_id");
				Long voyage_id = rs.getLong("voyage_id");

				Reservation reservation = new Reservation();
				reservation.setNumero(num_reservation);
				reservation.setStatut(StatutReserv.valueOf(statut));
				reservation.setTarif(tarif);
				reservation.setTauxTVA(taux_tva);
				reservation.setDateReservation(date_reservation);
				
				if (passager_id != null) {
					Passager passager = Application.getInstance().getPassagerDao().findById(passager_id);
					reservation.setPassager(passager);
				}
				if (client_id != null) {
					Client client = Application.getInstance().getClientDao().findById(client_id);
					reservation.setClient(client);
				}
				if (voyage_id != null) {
					Voyage voyage = Application.getInstance().getVoyageDao().findById(voyage_id);
					reservation.setVoyage(voyage);
				}
				
				reservations.add(reservation);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return reservations;
	}

	@Override
	public Reservation findById(String num_reservation) {
		Reservation reservation = null;
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT statut, tarif, taux_tva, date_reservation, passager_id, client_id, voyage_id FROM reservations WHERE num_reservation = ?");

			ps.setString(1, num_reservation);
			
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				String statut = rs.getString("statut");
				Float tarif = rs.getFloat("tarif");
				Float taux_tva = rs.getFloat("taux_tva");
				Date date_reservation = rs.getDate("date_reservation");
				Long passager_id = rs.getLong("passager_id");
				Long client_id = rs.getLong("client_id");
				Long voyage_id = rs.getLong("voyage_id");

				reservation = new Reservation();
				reservation.setNumero(num_reservation);
				reservation.setStatut(StatutReserv.valueOf(statut));
				reservation.setTarif(tarif);
				reservation.setTauxTVA(taux_tva);
				reservation.setDateReservation(date_reservation);
				
				if (passager_id != null) {
					Passager passager = Application.getInstance().getPassagerDao().findById(passager_id);
					reservation.setPassager(passager);
				}
				if (client_id != null) {
					Client client = Application.getInstance().getClientDao().findById(client_id);
					reservation.setClient(client);
				}
				if (voyage_id != null) {
					Voyage voyage = Application.getInstance().getVoyageDao().findById(voyage_id);
					reservation.setVoyage(voyage);
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return reservation;
	}

	@Override
	public void create(Reservation obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("INSERT INTO reservations (num_reservation, statut, tarif, taux_tva, date_reservation, passager_id, client_id, voyage_id) VALUES (?,?,?,?,?,?,?,?)");
			
			ps.setString(1, obj.getNumero());
			ps.setString(2, obj.getStatut().toString());
			ps.setFloat(3, obj.getTarif());
			ps.setFloat(4, obj.getTauxTVA());
			ps.setDate(5, new Date(obj.getDateReservation().getTime()));
			
			if(obj.getPassager() != null && obj.getPassager().getId() != null) {
				ps.setLong(6, obj.getPassager().getId());
			} else {
				ps.setNull(6, Types.NUMERIC);
			}
			if(obj.getClient() != null && obj.getClient().getClientId() != null) {
				ps.setLong(7, obj.getClient().getClientId());
			} else {
				ps.setNull(7, Types.NUMERIC);
			}
			if(obj.getVoyage() != null && obj.getVoyage().getId() != null) {
				ps.setLong(8, obj.getVoyage().getId());
			} else {
				ps.setNull(8, Types.NUMERIC);
			}
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void update(Reservation obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("UPDATE reservations SET statut = ?, tarif = ?, taux_tva = ?, date_reservation = ?, passager_id = ?, client_id = ?, voyage_id = ?  WHERE num_reservation = ?");
			
			ps.setString(1, obj.getStatut().toString());
			ps.setFloat(2, obj.getTarif());
			ps.setFloat(3, obj.getTauxTVA());
			ps.setDate(4, new Date(obj.getDateReservation().getTime()));

			if(obj.getPassager() != null && obj.getPassager().getId() != null) {
				ps.setLong(5, obj.getPassager().getId());
			} else {
				ps.setNull(5, Types.NUMERIC);
			}
			if(obj.getClient() != null && obj.getClient().getClientId() != null) {
				ps.setLong(6, obj.getClient().getClientId());
			} else {
				ps.setNull(6, Types.NUMERIC);
			}
			if(obj.getVoyage() != null && obj.getVoyage().getId() != null) {
				ps.setLong(7, obj.getVoyage().getId());
			} else {
				ps.setNull(7, Types.NUMERIC);
			}
			
			ps.setString(8, obj.getNumero());
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void delete(Reservation obj) {
		deleteById(obj.getNumero());
	}

	@Override
	public void deleteById(String id) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE reservations WHERE num_reservation = ?");

			ps.setString(1, id);
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
