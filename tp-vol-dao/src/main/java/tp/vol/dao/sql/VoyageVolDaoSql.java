package tp.vol.dao.sql;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tp.vol.Voyage;
import tp.vol.VoyageVol;
import tp.vol.dao.IVoyageVolDao;
import tp.vol.singleton.Application;

public class VoyageVolDaoSql implements IVoyageVolDao {

	@Override
	public List<VoyageVol> findAll() {
		List<VoyageVol> voyageVols = new ArrayList<VoyageVol>();

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection
					.prepareStatement("SELECT voyagevol_id, voyage_id, vol_id, ordre FROM voyagevol ");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Long idVoyageVol = rs.getLong("voyagevol_id");
				Long idVoyage = rs.getLong("voyage_id");
				Long idVol = rs.getLong("vol_id");
				int ordre = rs.getInt("ordre");

				VoyageVol voyageVol = new VoyageVol();
				voyageVol.setId(idVoyageVol);
//				voyageVol.setVoyage(idVoyage);
//				voyageVol.setVol(idVol);
				voyageVol.setOrdre(ordre);

				voyageVols.add(voyageVol);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return voyageVols;
	}

	@Override
	public VoyageVol findById(Long id) {

		VoyageVol voyagevol = null;

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement(
					"SELECT voyagevol_id, voyage_id, vol_id, ordre FROM voyagevol WHERE voyagevol_id = ?");

			ps.setLong(1, id);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				Long idVoyage = rs.getLong("voyage_id");
				Long idVol = rs.getLong("vol_id");
				int ordre = rs.getInt("ordre");

				voyagevol = new VoyageVol();
				voyagevol.setId(id);
//				voyagevol.setVoyage(voyage);
//				voyagevol.setVol(vol);
				voyagevol.setOrdre(ordre);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return voyagevol;
	}

	@Override
	public void create(VoyageVol obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection
					.prepareStatement("INSERT INTO voyagevol (voyagevol_id, voyage_id, vol_id, ordre) "
							+ "											VALUES (?,?,?,?)");

			ps.setLong(1, obj.getId());
//			ps.setLong(2, obj.getVoyage());
//			ps.setLong(3, obj.getVol());
			ps.setInt(4, obj.getOrdre());

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void update(VoyageVol obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement(
					"UPDATE voyagevol SET  voyage_id = ? , vol_id = ?, ordre = ? WHERE voyagevol_id = ? ");

//			ps.setLong(1, obj.getVosyage());
//			ps.setLong(2, obj.getVol());
			ps.setInt(3, obj.getOrdre());
			ps.setLong(4, obj.getId());

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void delete(VoyageVol obj) {
		deleteById(obj.getId());

	}

	@Override
	public void deleteById(Long id) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE voyagevol WHERE voyagevol_id = ?");

			ps.setLong(1, id);
			

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
