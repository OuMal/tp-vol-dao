package tp.vol.dao.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tp.vol.Ville;
import tp.vol.dao.IVilleDao;
import tp.vol.singleton.Application;

public class VilleDaoSql implements IVilleDao{

	@Override
	public List<Ville> findAll() {
		
		List<Ville> villes = new ArrayList<Ville>();
		
		Connection connection = null;
		
		try {
			connection = Application.getInstance().getConnection();
			
			PreparedStatement ps = connection.prepareStatement("SELECT nom_ville FROM villes");
			
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				String nom_ville = rs.getString("nom_ville");

				Ville ville = new Ville();
				ville.setNom(nom_ville);
				
				villes.add(ville);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return villes;
	
	}

	@Override
	public Ville findById(String id) {
		
		Ville ville = null;
		
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT nom_ville FROM villes WHERE nom_ville = ?");

			ps.setString(1, id);
			
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				String nom_ville = rs.getString("nom_ville");

				ville = new Ville();
				ville.setNom(nom_ville);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return ville;
	}

	@Override
	public void create(Ville obj) {
		
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("INSERT INTO villes (nom_ville) VALUES (?)");
			
			ps.setString(1, obj.getNom());

			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void update(Ville obj) {
		
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("UPDATE villes SET nom_ville = ? WHERE nom_ville = ?");
			
			ps.setString(1, obj.getNom());
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void delete(Ville obj) {
		deleteById(obj.getNom());
	}

	@Override
	public void deleteById(String id) {
		
		Connection connection = null;
		
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE villes WHERE nom_ville = ?");

			ps.setString(1, id);
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
