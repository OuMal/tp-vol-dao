package tp.vol.dao.sql;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import tp.vol.Aeroport;
import tp.vol.Compagnie;
import tp.vol.Vol;
import tp.vol.dao.IVolDao;
import tp.vol.singleton.Application;

public class VolDaoSql implements IVolDao {

	@Override
	public List<Vol> findAll() {
		List<Vol> vols = new ArrayList<Vol>();

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection
					.prepareStatement("SELECT vol_id, date_depart, date_arrivee, ouvert, nb_places, depart_code, arrivee_code, nom_compagnie FROM vols");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Long id = rs.getLong("vol_id");
				Date dateDepart = rs.getDate("date_depart");
				Date dateArrivee = rs.getDate("date_arrivee");
				String ouvert = rs.getString("ouvert");
				int nbPlaces = rs.getInt("nb_places");
				String codeDepart = rs.getString("depart_code");
				String codeArrivee = rs.getString("arrivee_code");
				String nomCompagnie = rs.getString("nom_compagnie");

				Vol vol = new Vol();
				vol.setId(id);
				vol.setDateDepart(dateDepart);
				vol.setDateArrivee(dateArrivee);
				vol.setOuvert(Boolean.parseBoolean(ouvert));
				vol.setNbPlaces(nbPlaces);
				
				if(nomCompagnie != null) {
					Compagnie compagnie = Application.getInstance().getCompagnieDao().findById(nomCompagnie);
					vol.setCompagnie(compagnie);
				} 
				
				if(codeDepart != null) {
					Aeroport aeroportDepart = Application.getInstance().getAeroportDao().findById(codeDepart);
					vol.setAeroportDepart(aeroportDepart);
				} 
				
				if(codeArrivee != null) {
					Aeroport aeroportArrivee = Application.getInstance().getAeroportDao().findById(codeArrivee);
					vol.setAeroportArrivee(aeroportArrivee);
				} 

				vols.add(vol);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return vols;

	}

	@Override
	public Vol findById(Long id) {
		Vol vol = null;

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement(
					"SELECT vol_id, date_depart, date_arrivee, ouvert, nb_places, depart_code, arrivee_code, nom_compagnie FROM vols WHERE vol_id = ?");

			ps.setLong(1, id);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				Date dateDepart = rs.getDate("date_depart");
				Date dateArrivee = rs.getDate("date_arrivee");
				String ouvert = rs.getString("ouvert");
				int nbPlaces = rs.getInt("nb_places");
				String codeDepart = rs.getString("depart_code");
				String codeArrivee = rs.getString("arrivee_code");
				String nomCompagnie = rs.getString("nom_compagnie");

				vol = new Vol();
				vol.setId(id);
				vol.setDateDepart(dateDepart);
				vol.setDateArrivee(dateArrivee);
				vol.setOuvert(Boolean.parseBoolean(ouvert));
				vol.setNbPlaces(nbPlaces);
				
				if(nomCompagnie != null) {
					Compagnie compagnie = Application.getInstance().getCompagnieDao().findById(nomCompagnie);
					vol.setCompagnie(compagnie);
				} 
				
				if(codeDepart != null) {
					Aeroport aeroportDepart = Application.getInstance().getAeroportDao().findById(codeDepart);
					vol.setAeroportDepart(aeroportDepart);
				} 
				
				if(codeArrivee != null) {
					Aeroport aeroportArrivee = Application.getInstance().getAeroportDao().findById(codeArrivee);
					vol.setAeroportArrivee(aeroportArrivee);
				} 

			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return vol;
	}

	@Override
	public void create(Vol obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection
					.prepareStatement("INSERT INTO vols (vol_id, date_depart, date_arrivee, ouvert, "
							+ "							 nb_places, depart_code, arrivee_code, nom_compagnie) "
							+ "											VALUES (?,?,?,?,?,?,?,?)");

			ps.setLong(1, obj.getId());
			ps.setDate(2, new Date(obj.getDateDepart().getTime()));
			ps.setDate(3, new Date(obj.getDateArrivee().getTime()));
			ps.setBoolean(4, obj.isOuvert());
			ps.setInt(5, obj.getNbPlaces());

			if (obj.getAeroportDepart() != null) {
				ps.setString(6, obj.getAeroportDepart().getCode());
			} else {
				ps.setNull(6,Types.VARCHAR);
			}
			
			if (obj.getAeroportArrivee() != null) {
				ps.setString(7, obj.getAeroportArrivee().getCode());
			} else {
				ps.setNull(7,Types.VARCHAR);
			}
			
			if (obj.getCompagnie() != null) {
				ps.setString(8, obj.getCompagnie().getNomCompagnie());
			} else {
				ps.setNull(8,Types.VARCHAR);
			}
			
			
			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void update(Vol obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement(
					"UPDATE vols SET date_depart = ?, date_arrivee = ?, ouvert = ?, nb_places = ?, depart_code = ?, arrivee_code = ?, nom_compagnie = ? WHERE vol_id = ?");

			ps.setDate(1, new Date(obj.getDateDepart().getTime()));
			ps.setDate(2, new Date(obj.getDateArrivee().getTime()));
			ps.setBoolean(3, obj.isOuvert());
			ps.setInt(4, obj.getNbPlaces());
			
			if (obj.getAeroportDepart() != null) {
				ps.setString(5, obj.getAeroportDepart().getCode());
			} else {
				ps.setNull(5,Types.VARCHAR);
			}
			
			if (obj.getAeroportArrivee() != null) {
				ps.setString(6, obj.getAeroportArrivee().getCode());
			} else {
				ps.setNull(6,Types.VARCHAR);
			}

			if (obj.getCompagnie() != null) {
				ps.setString(7, obj.getCompagnie().getNomCompagnie());
			} else {
				ps.setNull(7,Types.VARCHAR);
			}
			
			ps.setLong(8, obj.getId());
			
			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void delete(Vol obj) {
		deleteById(obj.getId());

	}

	@Override
	public void deleteById(Long id) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE vols WHERE vol_id = ?");

			ps.setLong(1, id);

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

}
