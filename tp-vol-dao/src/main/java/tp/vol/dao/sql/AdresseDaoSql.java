package tp.vol.dao.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tp.vol.Adresse;
import tp.vol.dao.IAdresseDao;
import tp.vol.singleton.Application;

public class AdresseDaoSql implements IAdresseDao{

	@Override
	public List<Adresse> findAll() {
		
		List<Adresse> adresses = new ArrayList<Adresse>();
		
		Connection connection = null;
		
		try {
			connection = Application.getInstance().getConnection();
			
			PreparedStatement ps = connection.prepareStatement("SELECT adresse_id, voie, complement, code_postal, ville, pays FROM adresses");
			
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				Long adresse_id = rs.getLong("adresse_id");
				String voie = rs.getString("voie");
				String complement = rs.getString("complement");
				String code_postal = rs.getString("code_postal");
				String ville = rs.getString("ville");
				String pays = rs.getString("pays");

				Adresse adresse = new Adresse();
				adresse.setAdresseId(adresse_id);
				adresse.setVoie(voie);
				adresse.setComplement(complement);
				adresse.setCodePostal(code_postal);
				adresse.setVille(ville);
				adresse.setPays(pays);
				
				adresses.add(adresse);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return adresses;
	
	}

	@Override
	public Adresse findById(Long id) {
		
		Adresse adresse = null;
		
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT adresse_id, voie, complement, code_postal, ville, pays FROM adresses WHERE adresse_id = ?");

			ps.setLong(1, id);
			
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				String voie = rs.getString("voie");
				String complement = rs.getString("complement");
				String code_postal = rs.getString("code_postal");
				String ville = rs.getString("ville");
				String pays = rs.getString("pays");

				adresse = new Adresse();
				adresse.setAdresseId(id);
				adresse.setVoie(voie);
				adresse.setComplement(complement);;
				adresse.setCodePostal(code_postal);
				adresse.setVille(ville);
				adresse.setPays(pays);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return adresse;
	}

	@Override
	public void create(Adresse obj) {
		
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("INSERT INTO adresses (adresse_id, voie, complement, code_postal, ville, pays) VALUES (?,?,?,?,?,?)");
			
			ps.setLong(1, obj.getAdresseId());
			ps.setString(2, obj.getVoie().toString());
			ps.setString(3, obj.getComplement().toString());
			ps.setString(4, obj.getCodePostal().toString());
			ps.setString(5, obj.getVille().toString());
			ps.setString(6, obj.getPays());

			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void update(Adresse obj) {
		
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("UPDATE adresses SET voie = ?, complement = ?, code_postal = ?, ville = ?, pays = ? WHERE adresse_id = ?");
			
			ps.setString(1, obj.getVoie());
			ps.setString(2, obj.getComplement());
			ps.setString(3, obj.getCodePostal());
			ps.setString(4,  obj.getVille());
			ps.setString(5, obj.getPays());
			ps.setLong(6, obj.getAdresseId());
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void delete(Adresse obj) {
		deleteById(obj.getAdresseId());
	}

	@Override
	public void deleteById(Long id) {
		
		Connection connection = null;
		
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE adresses WHERE adresse_id = ?");

			ps.setLong(1, id);
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
