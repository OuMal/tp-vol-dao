package tp.vol.dao.sql;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import tp.vol.Adresse;
import tp.vol.Passager;
import tp.vol.dao.IPassagerDao;
import tp.vol.enumeration.Civilite;
import tp.vol.enumeration.TypePI;
import tp.vol.singleton.Application;

public class PassagerDaoSql implements IPassagerDao{

	@Override
	public List<Passager> findAll() {
		List<Passager> passagers = new ArrayList<Passager>();

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT passager_id, nom, prenoms, dtnaissance, numidentite, nationalite, civilite, typepi, dtvalidite, mail, telephone, adresse_principale FROM passagers");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Long id = rs.getLong("passager_id");
				String nom = rs.getString("nom");
				String prenom = rs.getString("prenoms");
				Date dt_naissance = rs.getDate("dtnaissance");
				String numidentite = rs.getString("numidentite");
				String nationalite = rs.getString("nationalite");
				String civilite = rs.getString("civilite");
				String typepi = rs.getString("typepi");
				Date dt_validite = rs.getDate("dtvalidite");
				String mail = rs.getString("mail");
				String telephone = rs.getString("telephone");
				Long passagerAdresse = rs.getLong("adresse_principale");

				Passager passager = new Passager();
				passager.setId(id);
				passager.setNom(nom);
				passager.setPrenoms(prenom);
				passager.setDtNaissance(dt_naissance);
				passager.setNumIdentite(numidentite);
				passager.setNationalite(nationalite);
				passager.setCivilite(Civilite.valueOf(civilite));
				passager.setTypePI(TypePI.valueOf(typepi));
				passager.setDateValiditePI(dt_validite);
				passager.setMail(mail);
				passager.setTelephone(telephone);
				
				if (passagerAdresse != null) {
					Adresse adresse_id = Application.getInstance().getAdresseDao().findById(passagerAdresse);
					passager.setAdressePrincipale(adresse_id);
				}
				
				passagers.add(passager);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return passagers;
	}

	@Override
	public Passager findById(Long id) {
		Passager passager = null;
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT nom, prenoms, dtnaissance, numidentite, nationalite, civilite, typepi, dtvalidite, mail, telephone, adresse_principale FROM passagers WHERE passager_id = ?");

			ps.setLong(1, id);
			
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				String nom = rs.getString("nom");
				String prenom = rs.getString("prenoms");
				Date dt_naissance = rs.getDate("dtnaissance");
				String numidentite = rs.getString("numidentite");
				String nationalite = rs.getString("nationalite");
				String civilite = rs.getString("civilite");
				String typepi = rs.getString("typepi");
				Date dt_validite = rs.getDate("dtvalidite");
				String mail = rs.getString("mail");
				String telephone = rs.getString("telephone");
				Long passagerAdresse = rs.getLong("adresse_principale");

				passager = new Passager();
				passager.setId(id);
				passager.setNom(nom);
				passager.setPrenoms(prenom);
				passager.setDtNaissance(dt_naissance);
				passager.setNumIdentite(numidentite);
				passager.setNationalite(nationalite);
				passager.setCivilite(Civilite.valueOf(civilite));
				passager.setTypePI(TypePI.valueOf(typepi));
				passager.setDateValiditePI(dt_validite);
				passager.setMail(mail);
				passager.setTelephone(telephone);
				
				if (passagerAdresse != null) {
					Adresse adresse_id = Application.getInstance().getAdresseDao().findById(passagerAdresse);
					passager.setAdressePrincipale(adresse_id);
				}
				
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return passager;
	}

	@Override
	public void create(Passager obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("INSERT INTO passagers (passager_id, nom, prenoms, dtnaissance, numidentite, nationalite, civilite, typepi, dtvalidite, mail, telephone, adresse_principale) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
			
			ps.setLong(1, obj.getId());
			ps.setString(2, obj.getNom());
			ps.setString(3, obj.getPrenoms());
			ps.setDate(4, new Date(obj.getDtNaissance().getTime()));
			ps.setString(5, obj.getNumIdentite());
			ps.setString(6, obj.getNationalite());
			ps.setString(7, obj.getCivilite().toString());
			ps.setString(8, obj.getTypePI().toString());
			ps.setDate(9, new Date(obj.getDateValiditePI().getTime()));
			ps.setString(10, obj.getMail());
			ps.setString(11, obj.getTelephone());
			
			if(obj.getAdressePrincipale() != null && obj.getAdressePrincipale().getAdresseId() != null) {
				ps.setLong(13, obj.getAdressePrincipale().getAdresseId());
			} else {
				ps.setNull(13, Types.INTEGER);
			}
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void update(Passager obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("UPDATE passagers SET nom = ?, prenoms = ?, dtnaissance = ?, numidentite = ?, nationalite = ?, civilite = ?, typepi = ?, dtvalidite = ?, mail = ?, telephone = ?, adresse_principale = ? WHERE passager_id = ? ");
			
			ps.setString(1, obj.getNom());
			ps.setString(2, obj.getPrenoms());
			ps.setDate(3, new Date(obj.getDtNaissance().getTime()));
			ps.setString(4, obj.getNumIdentite());
			ps.setString(5, obj.getNationalite());
			ps.setString(6, obj.getCivilite().toString());
			ps.setString(7, obj.getTypePI().toString());
			ps.setDate(8, new Date(obj.getDateValiditePI().getTime()));
			ps.setString(9, obj.getMail());
			ps.setString(10, obj.getTelephone());
			ps.setLong(11, obj.getId());
			
			if(obj.getAdressePrincipale() != null && obj.getAdressePrincipale().getAdresseId() != null) {
				ps.setLong(13, obj.getAdressePrincipale().getAdresseId());
			} else {
				ps.setNull(13, Types.INTEGER);
			}
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void delete(Passager obj) {
		deleteById(obj.getId());
		
	}

	@Override
	public void deleteById(Long id) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE passagers WHERE passager_id = ?");

			ps.setLong(1, id);
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
