package tp.vol;

public class VoyageVol {
	
	private Long id;
	private int ordre;
	private Vol vol;
	private Voyage voyage;

	public VoyageVol() {

	}

	public VoyageVol(Vol vol, Voyage voyage) {
		super();
		this.vol = vol;
		this.voyage = voyage;
	}

	public int getOrdre() {
		return ordre;
	}

	public void setOrdre(int ordre) {
		this.ordre = ordre;
	}

	public Vol getVol() {
		return vol;
	}

	public void setVol(Vol vol) {
		this.vol = vol;
	}

	public Voyage getVoyage() {
		return voyage;
	}

	public void setVoyage(Voyage voyage) {
		this.voyage = voyage;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
