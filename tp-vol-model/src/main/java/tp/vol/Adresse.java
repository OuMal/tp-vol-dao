package tp.vol;

public class Adresse {
	private Long adresseId;
	private String voie;
	private String complement;
	private String codePostal;
	private String ville;
	private String pays;

	public Adresse() {

	}

	public Adresse(String voie, String complement, String codePostal, String ville, String pays) {
		super();
		this.voie = voie;
		this.complement = complement;
		this.codePostal = codePostal;
		this.ville = ville;
		this.pays = pays;
	}
	
	public Long getAdresseId() {
		return adresseId;
	}
	public void setAdresseId(Long adresse_id) {
		this.adresseId = adresse_id;
	}

	public String getVoie() {
		return voie;
	}

	public void setVoie(String voie) {
		this.voie = voie;
	}

	public String getComplement() {
		return complement;
	}

	public void setComplement(String complement) {
		this.complement = complement;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getPays() {
		return pays;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}

}
