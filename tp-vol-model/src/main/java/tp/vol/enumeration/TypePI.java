package tp.vol.enumeration;

public enum TypePI {
	CI("Carte d'identité"), PASS("Passeport");

	private final String label;

	private TypePI(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
}
