package tp.vol.enumeration;

public enum TypeClient {
	PRO("Client pro"), PART("Client particulier");

	private final String label;

	private TypeClient(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
}
