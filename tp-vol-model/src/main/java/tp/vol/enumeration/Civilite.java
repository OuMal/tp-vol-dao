package tp.vol.enumeration;

public enum Civilite {
	M("Monsieur"), MME("Madame"), MLLE("Mademoiselle"), NSP("Ne se prononce pas");

	private final String label;

	private Civilite(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
}
