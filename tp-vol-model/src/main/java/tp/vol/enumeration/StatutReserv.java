package tp.vol.enumeration;

public enum StatutReserv {
	ATT("En attente"), PAY("Payée"), ANN("Annulée");

	private final String label;

	private StatutReserv(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
}
