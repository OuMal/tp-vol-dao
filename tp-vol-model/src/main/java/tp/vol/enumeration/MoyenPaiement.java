package tp.vol.enumeration;

public enum MoyenPaiement {
	CB("Carte Bleue"), CH("Chèque"), ESP("Espèces");

	private final String label;

	private MoyenPaiement(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
}
