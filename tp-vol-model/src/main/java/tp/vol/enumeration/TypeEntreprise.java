package tp.vol.enumeration;

public enum TypeEntreprise {
	SA("Société Anonyme"),EI("Entreprise Individuel"),SAS("");

	private final String label;

	private TypeEntreprise(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
}
