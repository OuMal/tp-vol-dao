package tp.vol;

import java.util.ArrayList;
import java.util.Date;

import tp.vol.enumeration.Civilite;
import tp.vol.enumeration.TypePI;

public class Passager extends Personne {

	private Long id;
	private String nom;
	private String prenoms;
	private Date dtNaissance;
	private String numIdentite;
	private String nationalite;
	private Civilite civilite;
	private TypePI typePI;
	private Date dateValiditePI;
	private ArrayList<Reservation> reservations = new ArrayList<Reservation>();

	public Passager() {

	}

	public Passager(String nom, String prenoms) {
		super();
		this.nom = nom;
		this.prenoms = prenoms;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenoms() {
		return prenoms;
	}

	public void setPrenoms(String prenoms) {
		this.prenoms = prenoms;
	}

	public Date getDtNaissance() {
		return dtNaissance;
	}

	public void setDtNaissance(Date dtNaissance) {
		this.dtNaissance = dtNaissance;
	}

	public String getNumIdentite() {
		return numIdentite;
	}

	public void setNumIdentite(String numIdentite) {
		this.numIdentite = numIdentite;
	}

	public String getNationalite() {
		return nationalite;
	}

	public void setNationalite(String nationalite) {
		this.nationalite = nationalite;
	}
	
	public Civilite getCivilite() {
		return civilite;
	}

	public void setCivilite(Civilite civilite) {
		this.civilite = civilite;
	}

	public TypePI getTypePI() {
		return typePI;
	}

	public void setTypePI(TypePI typePI) {
		this.typePI = typePI;
	}

	public Date getDateValiditePI() {
		return dateValiditePI;
	}

	public void setDateValiditePI(Date dateValiditePI) {
		this.dateValiditePI = dateValiditePI;
	}

	public ArrayList<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(ArrayList<Reservation> reservations) {
		this.reservations = reservations;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


}
