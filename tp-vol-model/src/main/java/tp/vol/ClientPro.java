package tp.vol;

import tp.vol.enumeration.TypeEntreprise;

public class ClientPro extends Client {
	private int numeroSIRET;
	private String nomEntreprise;
	private String numTVA;
	private TypeEntreprise typeEntreprise;

	public ClientPro() {

	}

	public ClientPro(String nomEntreprise) {
		super();
		this.nomEntreprise = nomEntreprise;
	}

	public int getNumeroSIRET() {
		return numeroSIRET;
	}

	public void setNumeroSIRET(int numeroSIRET) {
		this.numeroSIRET = numeroSIRET;
	}

	public String getNomEntreprise() {
		return nomEntreprise;
	}

	public void setNomEntreprise(String nomEntreprise) {
		this.nomEntreprise = nomEntreprise;
	}

	public String getNumTVA() {
		return numTVA;
	}

	public void setNumTVA(String numTVA) {
		this.numTVA = numTVA;
	}

	public TypeEntreprise getTypeEntreprise() {
		return typeEntreprise;
	}

	public void setTypeEntreprise(TypeEntreprise typeEntreprise) {
		this.typeEntreprise = typeEntreprise;
	}

}
