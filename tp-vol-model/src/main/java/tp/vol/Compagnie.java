package tp.vol;

import java.util.ArrayList;

public class Compagnie {

	private String nomCompagnie;
	private ArrayList<Vol> vols = new ArrayList<Vol>();

	public Compagnie() {

	}

	public Compagnie(String nomCompagnie) {
		super();
		this.nomCompagnie = nomCompagnie;
	}

	public String getNomCompagnie() {
		return nomCompagnie;
	}

	public void setNomCompagnie(String nomCompagnie) {
		this.nomCompagnie = nomCompagnie;
	}

	public ArrayList<Vol> getVols() {
		return vols;
	}

	public void setVols(ArrayList<Vol> vols) {
		this.vols = vols;
	}

}
