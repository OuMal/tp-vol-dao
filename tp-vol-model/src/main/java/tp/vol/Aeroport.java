package tp.vol;

import java.util.ArrayList;

public class Aeroport {
	private String code;
	private ArrayList<Ville> villes = new ArrayList<Ville>();
	private ArrayList<Vol> volsDepart = new ArrayList<Vol>();
	private ArrayList<Vol> volsArrivee = new ArrayList<Vol>();

	public Aeroport() {

	}

	public Aeroport(String code) {
		super();
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public ArrayList<Ville> getVilles() {
		return villes;
	}

	public void setVilles(ArrayList<Ville> villes) {
		this.villes = villes;
	}

	public ArrayList<Vol> getVolsDepart() {
		return volsDepart;
	}

	public void setVolsDepart(ArrayList<Vol> volsDepart) {
		this.volsDepart = volsDepart;
	}

	public ArrayList<Vol> getVolsArrivee() {
		return volsArrivee;
	}

	public void setVolsArrivee(ArrayList<Vol> volsArrivee) {
		this.volsArrivee = volsArrivee;
	}

}
