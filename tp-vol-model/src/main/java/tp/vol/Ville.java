package tp.vol;

import java.util.ArrayList;

public class Ville {
	private String nom;
	private ArrayList<Aeroport> aeroports = new ArrayList<Aeroport>();

	public Ville() {

	}

	public Ville(String nom) {
		super();
		this.nom = nom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public ArrayList<Aeroport> getAeroports() {
		return aeroports;
	}

	public void setAeroports(ArrayList<Aeroport> aeroports) {
		this.aeroports = aeroports;
	}

}
