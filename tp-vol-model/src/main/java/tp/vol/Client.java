package tp.vol;

import java.util.ArrayList;

import tp.vol.enumeration.Civilite;
import tp.vol.enumeration.MoyenPaiement;
import tp.vol.enumeration.TypeClient;
import tp.vol.enumeration.TypeEntreprise;

public class Client extends Personne {
	
	private Long clientId;
	private String mail;
	private String telephone;
	private MoyenPaiement moyenPaiement;
	private Civilite civilite;
	private String nom;
	private String prenom;
	private String numSiret;
	private String nomEntreprise;
	private String numTva;
	private TypeEntreprise typeEntreprise;
	private TypeClient typeClient;
	//private ArrayList<Reservation> reservations = new ArrayList<Reservation>();
	private Adresse adresseFacturation;

	public Long getClientId() {
		return clientId;
	}
	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public MoyenPaiement getMoyenPaiement() {
		return moyenPaiement;
	}
	public void setMoyenPaiement(MoyenPaiement moyenPaiement) {
		this.moyenPaiement = moyenPaiement;
	}
	public Civilite getCivilite() {
		return civilite;
	}
	public void setCivilite(Civilite civilite) {
		this.civilite = civilite;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getNumSiret() {
		return numSiret;
	}
	public void setNumSiret(String numSiret) {
		this.numSiret = numSiret;
	}
	public String getNomEntreprise() {
		return nomEntreprise;
	}
	public void setNomEntreprise(String nomEntreprise) {
		this.nomEntreprise = nomEntreprise;
	}
	public String getNumTva() {
		return numTva;
	}
	public void setNumTva(String numTva) {
		this.numTva = numTva;
	}
	public TypeEntreprise getTypeEntreprise() {
		return typeEntreprise;
	}
	public void setTypeEntreprise(TypeEntreprise typeEntreprise) {
		this.typeEntreprise = typeEntreprise;
	}
	public TypeClient getTypeClient() {
		return typeClient;
	}
	public void setTypeClient(TypeClient typeClient) {
		this.typeClient = typeClient;
	}
	public Adresse getAdresseFacturation() {
		return adresseFacturation;
	}
	public void setAdresseFacturation(Adresse adresseFacturation) {
		this.adresseFacturation = adresseFacturation;
	}
	

	
}
