package tp.vol;

import java.util.ArrayList;
import java.util.Date;

public class Vol {
	private Long id;
	private Date dateDepart;
	private Date dateArrivee;
	private int numero;
	private boolean ouvert;
	private int nbPlaces;
	private Aeroport aeroportDepart;
	private Aeroport aeroportArrivee;
	private Compagnie compagnie;
	private ArrayList<VoyageVol> voyagesvols = new ArrayList<VoyageVol>();

	// Constructeurs
	public Vol() {

	}

	public Vol(int numero) {
		super();
		this.numero = numero;
	}

	public Vol(int numero, Aeroport aeroportDepart, Aeroport aeroportArrivee, Compagnie compagnie) {
		super();
		this.numero = numero;
		this.aeroportDepart = aeroportDepart;
		this.aeroportArrivee = aeroportArrivee;
		this.compagnie = compagnie;
	}

	// Getters et setters
	public Date getDateDepart() {
		return dateDepart;
	}

	public void setDateDepart(Date dateDepart) {
		this.dateDepart = dateDepart;
	}

	public Date getDateArrivee() {
		return dateArrivee;
	}

	public void setDateArrivee(Date dateArrivee) {
		this.dateArrivee = dateArrivee;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public boolean isOuvert() {
		return ouvert;
	}

	public void setOuvert(boolean ouvert) {
		this.ouvert = ouvert;
	}

	public int getNbPlaces() {
		return nbPlaces;
	}

	public void setNbPlaces(int nbPlaces) {
		this.nbPlaces = nbPlaces;
	}

	public Aeroport getAeroportDepart() {
		return aeroportDepart;
	}

	public void setAeroportDepart(Aeroport aeroportDepart) {
		this.aeroportDepart = aeroportDepart;
	}

	public Aeroport getAeroportArrivee() {
		return aeroportArrivee;
	}

	public void setAeroportArrivee(Aeroport aeroportArrivee) {
		this.aeroportArrivee = aeroportArrivee;
	}

	public Compagnie getCompagnie() {
		return compagnie;
	}

	public void setCompagnie(Compagnie compagnie) {
		this.compagnie = compagnie;
	}

	public ArrayList<VoyageVol> getVoyagesvols() {
		return voyagesvols;
	}

	public void setVoyagesvols(ArrayList<VoyageVol> voyagesvols) {
		this.voyagesvols = voyagesvols;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
