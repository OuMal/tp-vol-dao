package tp.vol;

import java.util.ArrayList;

public class Voyage {
	
	private Long id;
	private ArrayList<VoyageVol> voyagesvols = new ArrayList<VoyageVol>();
	private ArrayList<Reservation> reservations = new ArrayList<Reservation>();

	public ArrayList<VoyageVol> getVoyagesvols() {
		return voyagesvols;
	}

	public void setVoyagesvols(ArrayList<VoyageVol> voyagesvols) {
		this.voyagesvols = voyagesvols;
	}

	public ArrayList<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(ArrayList<Reservation> reservations) {
		this.reservations = reservations;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
