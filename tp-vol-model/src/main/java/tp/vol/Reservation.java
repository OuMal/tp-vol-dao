package tp.vol;

import java.util.Date;

import tp.vol.enumeration.StatutReserv;

public class Reservation {

	private String numero;
	private StatutReserv statut;
	private float tarif;
	private float tauxTVA;
	private Date dateReservation;
	private Voyage voyage;
	private Client client;
	private Passager passager;

	public Reservation() {

	}

	public Reservation(String numero, Voyage voyage, Passager passager, Client client) {
		super();
		this.numero = numero;
		this.voyage = voyage;
		this.passager = passager;
		this.client = client;
	}

	public Reservation(String numero, Voyage voyage, Client client) {
		super();
		this.numero = numero;
		this.voyage = voyage;
		this.client = client;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public StatutReserv getStatut() {
		return statut;
	}

	public void setStatut(StatutReserv statut) {
		this.statut = statut;
	}

	public float getTarif() {
		return tarif;
	}

	public void setTarif(float tarif) {
		this.tarif = tarif;
	}

	public float getTauxTVA() {
		return tauxTVA;
	}

	public void setTauxTVA(float tauxTVA) {
		this.tauxTVA = tauxTVA;
	}

	public Date getDateReservation() {
		return dateReservation;
	}

	public void setDateReservation(Date dateReservation) {
		this.dateReservation = dateReservation;
	}

	public Voyage getVoyage() {
		return voyage;
	}

	public void setVoyage(Voyage voyage) {
		this.voyage = voyage;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Passager getPassager() {
		return passager;
	}

	public void setPassager(Passager passager) {
		this.passager = passager;
	}

}
